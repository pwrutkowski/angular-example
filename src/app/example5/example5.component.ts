import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-example5',
  templateUrl: './example5.component.html'
})
export class Example5Component implements OnInit {
  myBoolean = true;
  myStringArray = ['abc', 'def', 'ghi', 'jkl'];

  ngOnInit(): void {
    setInterval(() => this.myBoolean = !this.myBoolean, 1000);
  }

}
