import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-example4',
  templateUrl: './example4.component.html',
})
export class Example4Component implements OnInit {

  myNumber = 123;

  constructor() {
    console.log('Obiekt komponentu został utworzony');
  }

  ngOnInit(): void {
    console.log('Komponent został zainicjalizowany');
    setInterval(() => this.myNumber++, 3000);
  }

}
