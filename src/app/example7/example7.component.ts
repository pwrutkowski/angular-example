import {Component} from '@angular/core';

@Component({
  selector: 'app-example7',
  templateUrl: './example7.component.html'
})
export class Example7Component {


  constructor() {
  }

  onButtonClick(): void {
    console.log('Kliknięto przycisk');
  }

  onFormSubmit(): void {
    console.log('Zatwierdzono formularz');
  }

  onTextInputChange(): void {
    console.log('Zmieniono wartość pola tektowego');
  }
}
